package main

import (
	"fmt"
	"github.com/brushknight/go-simple-bank/server"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

const appPort = "9876"

func main() {

	r := mux.NewRouter()

	r.HandleFunc("/api/v1/account/{uuid}", server.HttpHandleGetAccount)
	r.HandleFunc("/api/v1/account/{uuid}/balance", server.HttpHandleGetAccountBalance)
	r.HandleFunc("/api/v1/accounts", server.HttpHandleGetAllAccounts)

	// curl -XPOST -H "Content-type: application/json" -d '{"fromAccountUuid": "daenerys_targaryen", "toAccountUuid": "jon_snow", "amount": 500.10}' 'http://localhost:9876/api/v1/funds/send'
	r.HandleFunc("/api/v1/funds/send", server.HttpHandleSendFunds)

	r.HandleFunc("/api/v1/transactions", server.HttpHandleGetAllTransactions)
	r.HandleFunc("/api/v1/transaction/confirm/{uuid}", server.HttpHandleConfirmTransaction)

	r.HandleFunc("/admin/api/v1/reset", server.HttpHandleAdminReset)

	log.Printf(`Starting server on %s port`, appPort)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(`:%s`, appPort), r))

}
