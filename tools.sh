#!/usr/bin/env bash

command=$1

show_hint(){
        cat << EOF

usage: bash tools.sh [command]
available commands:
    - build                     >>> build binary for the dev env
    - build-stage               >>> build binary for the stage env
    - run                       >>> run binary for the dev env
    - run-stage                 >>> run binary for the stage env
    - dep                       >>> go get -t -v ./...
    - gofmt                     >>> gofmt && commit
    - gofmt-travis              >>> gofmt check only
    - tests                     >>> run tests

Report bugs to: brushknight@gmail.com
EOF

}

build(){
    [ -e ./build/ ] && rm -rf ./build/**

    go build -o ./build/main.bin ./main.go &&\
      chmod +x ./build/main.bin
}

build_stage(){
    go build -o /bin/app.bin ./main.go &&\
      chmod +x /bin/app.bin
}

run(){
    build/main.bin
}

run_stage(){
    /bin/app.bin
}

gofmt(){
    ./tools.gofmt.sh
}

gofmt_travis(){
    if [ -n "$(gofmt -l .)" ]; then
      echo "Go code is not formatted:"
      gofmt -d .
      exit 1
    fi

    exit 0
}

tests(){
    go test -v ./...
}

install_deps(){
    go get -t -v ./...
}

case $command in
    "build")
        build
    ;;
    "build-stage")
        build_stage
    ;;
    "run")
        run
    ;;
    "run-stage")
        run_stage
    ;;
    "gofmt")
        gofmt
    ;;
    "gofmt-travis")
        gofmt_travis
    ;;
    "tests")
        tests
    ;;
    "dep")
        install_deps
    ;;
    "dev")
        tests
        build
        run
    ;;
    "ci")
        install_deps
        tests
        build_stage
        run_stage
    ;;

    *)
        show_hint
        exit 1
esac