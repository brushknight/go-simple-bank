# go-simple-bank ![tests](https://travis-ci.org/brushknight/go-simple-bank.svg?branch=master)
Demo endpoint: https://verse-demo.brushknight.com


## How it works?

1. Before start, you need to reset database. (you can find all api methods in tha table below)
2. On the next step you should check available accounts and choose two accounts to make a transaction
3. Create transaction and get transaction UUID
4. When transaction created, you can confirm it with UUID

## API documentation
Method | URI | Body | Comment
--- | --- | ---| ---
GET | /api/v1/account/`{uuid:string}` | nil | get info about one account
GET | /api/v1/accounts | nil | ge list of accounts
GET | /api/v1/transactions | nil |  ge list of transactions
POST | /api/v1/funds/send | ```{"fromAccountUuid": string, "toAccountUuid": string, "amount": float}``` | create new transaction
PUT | /api/v1/transaction/`{uuid:string}`/confirm | nil | confirm transaction
GET | /admin/api/v1/reset/ | nil | Reset database

## todo
- create new account
- rollback transaction
- credit account
- round float up to 2 decimals
- format HTTP responses
- use golang:alpine
- extend error codes and messages
- cancel transaction
