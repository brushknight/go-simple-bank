package server

import "sync"

type Database struct {
	Accounts     map[string]*Account     `json:"accounts"`
	Transactions map[string]*Transaction `json:"transactions"`
}

const maxLengthOfMaps = 1000

var instance *Database
var mu sync.Mutex

func GetDatabase() *Database {
	if instance == nil {
		mu.Lock()
		defer mu.Unlock()

		if instance == nil {
			instance = &Database{}
			instance.init()
			instance.initTestData()
		}
	}
	return instance
}

func ResetDatabase() {
	GetDatabase().reset()
}

func (db *Database) init() {
	db.Accounts = make(map[string]*Account, maxLengthOfMaps)
	db.Transactions = make(map[string]*Transaction, maxLengthOfMaps)
}

func (db *Database) initTestData() {
	db.Accounts["daenerys_targaryen"] = NewAccount("daenerys_targaryen", "Daenerys Targaryen", 100000, 0)
	db.Accounts["jon_snow"] = NewAccount("jon_snow", "Jon Snow", 0, 0)
	db.Accounts["Cersei cersei_lannister"] = NewAccount("cersei_lannister", "Cersei Lannister", 500000, 0)
	db.Accounts["tyrion_lannister"] = NewAccount("tyrion_lannister", "Tyrion Lannister", 500, 0)
	db.Accounts["ygritte"] = NewAccount("ygritte", "Ygritte", 0, 0)
}

func (db *Database) reset() {
	db.init()
	db.initTestData()
}
