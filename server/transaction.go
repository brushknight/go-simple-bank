package server

import (
	"errors"
	"fmt"
	"time"
)

const StatusPending int = 1
const StatusConfirmed int = 100
const StatusCancelled int = 2

type Transaction struct {
	UUID            string  `json:"uuid"`
	FromAccountUuid string  `json:"fromAccountUuid"`
	ToAccountUuid   string  `json:"toAccountUuid"`
	Amount          float64 `json:"amount"`
	Status          int     `json:"status"`
	CreatedAt       int64   `json:"createdAt"`
	CompletedAt     int64   `json:"completedAt"`
}

func (tx *Transaction) Confirm() {
	tx.Status = StatusConfirmed
	tx.CompletedAt = time.Now().Unix()
}

func NewPendingTransaction(uuid string, fromAccountUuid string, toAccountUuid string, amount float64) *Transaction {
	return NewTransaction(uuid, fromAccountUuid, toAccountUuid, amount, StatusPending, time.Now().Unix(), 0)
}

func NewTransaction(uuid string, fromAccountUuid string, toAccountUuid string, amount float64, status int, createdAt int64, completedAt int64) *Transaction {
	transaction := new(Transaction)
	transaction.UUID = uuid
	transaction.FromAccountUuid = fromAccountUuid
	transaction.ToAccountUuid = toAccountUuid
	transaction.Amount = amount
	transaction.Status = status
	transaction.CreatedAt = createdAt
	transaction.CompletedAt = completedAt
	return transaction
}

func GetAllTransactions() map[string]*Transaction {
	return GetDatabase().Transactions
}

func GetOneTransactionByUuid(uuid string) (transaction *Transaction, err error) {

	transactions := GetAllTransactions()

	if transaction, ok := transactions[uuid]; ok {
		return transaction, nil
	}

	return nil, errors.New(fmt.Sprintf(`Transaction with uuid %s not found`, uuid))
}

func SaveTransaction(transaction *Transaction) {
	GetDatabase().Transactions[transaction.UUID] = transaction
}
