package server

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

type SendMoneyRequest struct {
	FromAccountUuid string  `json:"fromAccountUuid"`
	ToAccountUuid   string  `json:"toAccountUuid"`
	Amount          float64 `json:"amount"`
}

func HttpHandleGetAccount(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "Method not allowed")
		return
	}

	vars := mux.Vars(r)

	account, err := GetOneAccountByUuid(vars["uuid"])

	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, err.Error())
		log.Println(err)
		return
	}

	accountAsJson, err := json.Marshal(account)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Internal server error")
		log.Println(err)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, `%s`, accountAsJson)
	return
}

func HttpHandleGetAccountBalance(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "Method not allowed")
		return
	}

	vars := mux.Vars(r)

	account, err := GetOneAccountByUuid(vars["uuid"])

	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, err.Error())
		log.Println(err)
		return
	}

	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, `{balance:%f, lochedFunds:%f}`, account.Balance, account.LockedFunds)
	return
}

func HttpHandleGetAllAccounts(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "Method not allowed")
		return
	}

	accounts := GetAllAccounts()
	accountsCount := len(accounts)

	if accountsCount == 0 {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "No accounts found")
		return
	} else {

		accountsAsJson, err := json.Marshal(accounts)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "Internal server error")
			log.Println(err)
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, `%s`, accountsAsJson)
		return
	}
}

func HttpHandleGetAllTransactions(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "Method not allowed")
		return
	}

	transactions := GetAllTransactions()
	accountsCount := len(transactions)

	if accountsCount == 0 {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "No transactions found")
		return
	} else {

		transactionsAsJson, err := json.Marshal(transactions)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "Internal server error")
			log.Println(err)
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, `%s`, transactionsAsJson)
		return
	}
}

func HttpHandleSendFunds(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "Method not allowed")
		return
	}

	decoder := json.NewDecoder(r.Body)
	var sendMoneyRequest SendMoneyRequest
	err := decoder.Decode(&sendMoneyRequest)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "Dab request")
		log.Println(err)
		return
	}

	transaction, err := SendFunds(sendMoneyRequest.FromAccountUuid, sendMoneyRequest.ToAccountUuid, sendMoneyRequest.Amount)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, err.Error())
		log.Println(err)
		return
	}

	transactionAsJson, err := json.Marshal(transaction)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Internal server error")
		log.Println(err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, `%s`, transactionAsJson)
	return
}

func HttpHandleConfirmTransaction(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "Method not allowed")
		return
	}

	vars := mux.Vars(r)

	transaction, err := ConfirmTransaction(vars["uuid"])

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, err.Error())
		log.Println(err)
		return
	}

	transactionAsJson, err := json.Marshal(transaction)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Internal server error")
		log.Println(err)
		return
	}

	w.WriteHeader(http.StatusCreated)
	fmt.Fprintf(w, `%s`, transactionAsJson)
	return
}

func HttpHandleAdminReset(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "Method not allowed")
		return
	}

	ResetDatabase()

	fmt.Fprintln(w, "Database reset success")
	log.Println("Database reset success")
	w.WriteHeader(http.StatusOK)

	return
}
