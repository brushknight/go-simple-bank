package server

import (
	"errors"
	"fmt"
)

type Account struct {
	UUID        string  `json:"uuid"`
	Title       string  `json:"title"`
	Balance     float64 `json:"balance"`
	LockedFunds float64 `json:"lockedFunds"`
}

func NewAccount(uuid string, title string, balance float64, lockedFunds float64) *Account {
	account := new(Account)
	account.UUID = uuid
	account.Title = title
	account.Balance = balance
	account.LockedFunds = lockedFunds
	return account
}

func (account *Account) IsEnoughFundsToLock(amount float64) bool {
	return account.Balance >= amount
}

func (account *Account) IsEnoughFundsToDebit(amount float64) bool {
	return account.Balance+account.LockedFunds >= amount
}

func (account *Account) LockFunds(amount float64) bool {
	if account.IsEnoughFundsToLock(amount) {
		account.Balance -= amount
		account.LockedFunds += amount
		return true
	}

	return false
}

func (account *Account) DebitFunds(amount float64) bool {
	if account.IsEnoughFundsToDebit(amount) {

		if account.LockedFunds >= amount {
			account.LockedFunds -= amount
			return true
		}

		restPartOfDebit := amount - account.LockedFunds
		account.LockedFunds = 0
		account.Balance -= restPartOfDebit
		return true
	}

	return false
}

func (account *Account) CreditFunds(amount float64) bool {
	if amount < 0 {
		return false
	}
	account.Balance += amount

	return true
}

func GetAllAccounts() map[string]*Account {
	return GetDatabase().Accounts
}

func GetOneAccountByUuid(uuid string) (account *Account, err error) {

	accounts := GetAllAccounts()

	if account, ok := accounts[uuid]; ok {
		return account, nil
	}

	return nil, errors.New(fmt.Sprintf(`Account with uuid %s not found`, uuid))
}

func SaveAccount(account *Account) {
	GetDatabase().Accounts[account.UUID] = account
}
