package server

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAccount_CreditFunds(t *testing.T) {

	account := NewAccount("1", "test account 1", 9.99, 0)
	isFundsCredited := account.CreditFunds(10.01)

	assert.True(t, isFundsCredited, "funds credited")
	assert.True(t, almostEqual(account.Balance, 20), "funds credited")
	assert.True(t, almostEqual(account.LockedFunds, 0), "funds credited")
}

func TestAccount_CreditFunds_belowZero(t *testing.T) {

	account := NewAccount("1", "test account 1", 9.99, 0)
	isFundsCredited := account.CreditFunds(-10.01)

	assert.False(t, isFundsCredited, "funds credited")
	assert.True(t, almostEqual(account.Balance, 9.99), "funds not credited")
	assert.True(t, almostEqual(account.LockedFunds, 0), "funds not credited")
}
