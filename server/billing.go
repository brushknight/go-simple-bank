package server

import (
	"errors"
	"github.com/satori/go.uuid"
	"log"
)

func SendFunds(fromAccountUuid string, toAccountUuid string, amount float64) (*Transaction, error) {

	fromAccount, err := GetOneAccountByUuid(fromAccountUuid)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	toAccount, err := GetOneAccountByUuid(toAccountUuid)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	transactionUuid := uuid.Must(uuid.NewV4())

	transaction := NewPendingTransaction(transactionUuid.String(), fromAccount.UUID, toAccount.UUID, amount)
	isFundsLocked := fromAccount.LockFunds(amount)

	if !isFundsLocked {
		log.Println("not enough funds")
		return nil, errors.New("not enough funds")
	}

	SaveAccount(fromAccount)
	SaveTransaction(transaction)

	return transaction, nil

}

func ConfirmTransaction(transactionId string) (*Transaction, error) {

	transaction, err := GetOneTransactionByUuid(transactionId)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	if transaction.Status != StatusPending {
		return nil, errors.New("you can not Confirm already processed transaction")
	}

	fromAccount, err := GetOneAccountByUuid(transaction.FromAccountUuid)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	toAccount, err := GetOneAccountByUuid(transaction.ToAccountUuid)

	if err != nil {
		log.Println(err)
		return nil, err
	}

	isFundsDebited := fromAccount.DebitFunds(transaction.Amount)

	if !isFundsDebited {
		return nil, errors.New("you have not enough funds")
	}

	isCredited := toAccount.CreditFunds(transaction.Amount)

	if !isCredited {
		return nil, errors.New("an amount cannot be below zero")
	}
	transaction.Confirm()

	SaveAccount(fromAccount)
	SaveAccount(toAccount)
	SaveTransaction(transaction)

	return transaction, nil

}
