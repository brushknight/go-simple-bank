package server

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAccount_IsEnoughFundsToLock(t *testing.T) {

	account := NewAccount("1", "test account 1", 10, 0)

	assert.False(t, account.IsEnoughFundsToLock(11), "10 < 11")
	assert.True(t, account.IsEnoughFundsToLock(9.99), "10 > 9.99")
	assert.True(t, account.IsEnoughFundsToLock(10), "10 == 10")
}

func TestAccount_LockFunds(t *testing.T) {

	account := NewAccount("1", "test account 1", 10, 0)
	isFundsLocked := account.LockFunds(5)

	assert.True(t, isFundsLocked, "10 > 5")
	assert.Equal(t, account.LockedFunds, float64(5), " 5")
	assert.Equal(t, account.Balance, float64(5), "10 - 5")
}

func TestAccount_LockFunds_notEnoughFunds(t *testing.T) {

	account := NewAccount("1", "test account 1", 42.42, 0)
	isFundsLocked := account.LockFunds(42.43)

	assert.False(t, isFundsLocked, "42.42 < 42.43")
	assert.Equal(t, account.LockedFunds, float64(0), "lock funds Failed")
	assert.True(t, almostEqual(account.Balance, 42.42), "lock funds Failed")
}
