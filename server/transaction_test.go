package server

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestNewPendingTransaction(t *testing.T) {

	transaction := NewPendingTransaction("1", "1", "2", 10)
	assert.Equal(t, transaction.Status, StatusPending, "Status of new transaction is pending")
	assert.Equal(t, transaction.CompletedAt, int64(0), "New transaction cannot be completed")
}

func TestTransaction_Confirm(t *testing.T) {

	transaction := NewPendingTransaction("1", "1", "2", 10)
	transaction.Confirm()
	assert.Equal(t, transaction.Status, StatusConfirmed, "Status of new confirmed transaction is confirmed - 100")
	assert.True(t, transaction.CompletedAt <= time.Now().Unix(), "Completed transaction has completion time older than now")
}
