package server

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAccount_IsEnoughFundsToDebit(t *testing.T) {

	account := NewAccount("1", "test account 1", 10, 10)

	assert.False(t, account.IsEnoughFundsToDebit(21), "10 + 10 < 21")
	assert.True(t, account.IsEnoughFundsToDebit(9.99), "10 + 10 > 9.99")
	assert.True(t, account.IsEnoughFundsToDebit(20), "10 + 10 == 20")
}

func TestAccount_DebitFunds(t *testing.T) {

	account := NewAccount("1", "test account 1", 10, 10)
	isFundsDebited := account.DebitFunds(19.99)

	assert.True(t, isFundsDebited, "10 > 5")
	assert.Equal(t, account.LockedFunds, float64(0), "10 - 19.99")
	assert.True(t, almostEqual(account.Balance, 0.01), "10 + 10 - 19.99")
}

func TestAccount_DebitFunds_equal(t *testing.T) {

	account := NewAccount("1", "test account 1", 10, 10)
	isFundsDebited := account.DebitFunds(20)

	assert.True(t, isFundsDebited, "10 > 5")
	assert.Equal(t, account.LockedFunds, float64(0), "unsigned(10 - 20)")
	assert.Equal(t, account.Balance, float64(0), "10 + 10 - 20")
}

func TestAccount_DebitFunds_notEnoughFunds(t *testing.T) {

	account := NewAccount("1", "test account 1", 10, 10)
	isFundsDebited := account.DebitFunds(20.01)

	assert.False(t, isFundsDebited, "10 + 10 < 20.01")
	assert.Equal(t, account.LockedFunds, float64(10), "debit funds Failed")
	assert.Equal(t, account.Balance, float64(10), "debit funds Failed")
}
